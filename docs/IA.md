
# (IA) Inteligencia Artificial 

son los bots con los que juega en STK

## IA local

En el modo multijugador para un jugador y en pantalla dividida, los juegos se pueden configurar con IA. Se pueden usar en cualquier modo de juego excepto Egg Hunt y cualquier dificultad. Para la mayoría de los jugadores, son bastante desafiantes e incluso algunos de los mejores jugadores tienen problemas para vencerlos. 

## IA online

en uegos en linea se pueden configurar bots, anteriormente STK Latinoamerica tenia bots en los servidores principales pero como no les gustan a gran parte de la comunidad, aun mas cuando se hace speedrun, decidimos quitarlos pero puede seguir usando los bots en linea en servidores como frankfurt one (tenga en cuenta que los bots en linea son mucho peores que los locales y pueden resultar muy faciles de vencer).

# usar las IA

## local

en local el modo historia siempre usa bots para todas sus carreras por lo que no necesitará activarlos manualmente, pero si va a jugar individualmente o multijugador local con pantalla dividida acuando seleccione la pista que va a jugar se le preguntan la cantidad de vueltas y la cantidad de jugadores controlados por el ordenador (bots)

## en linea

para juegos en linea es mas complicado ya que necesita ser administrador del servidor donde quiere poner bots o pedirle al administrador que los añada por usted, para poder añadir bots a un servidor en linea necesita acceso a la terminal del servidor y necesita conocer el puerto donde se ejecuta el servidor que quiere usar.

el comando para añadir bots al servidor es el sicuente

 - supertuxkart --connect-now=127.0.0.1:puerto del servidor --network-ai=numero de bots
 - supertuxkart --connect-now=127.0.0.1:2759 --network-ai=2




gracias a kimden por el contenido original