# Welcome to MkDocs

For full documentation visit [mkdocs.org](https://www.mkdocs.org).

## Commands

* `mkdocs new [dir-name]` - Create a new project.
* `mkdocs serve` - Start the live-reloading docs server.
* `mkdocs build` - Build the documentation site.
* `mkdocs -h` - Print help message and exit.

## How to add to this site

* Create an account on gitlab.com
* Tell UnsolvedCypher your username so your account can be added to the project
* Make sure `git` is installed on your computer
* Make sure Python 3 or later is installed on your computer
* In your terminal type `git clone git@gitlab.com:UnsolvedCypher/stk-latinoamerica-wiki.git`
* Enter your username/password from GitLab
* You will have a folder called `stk-latinoamerica-wiki`
* Go into the folder in your terminal
* run `pip install -r requirements.txt` (the command may be called `pip3` depending on your OS)
* Type `mkdocs serve` and visit `localhost:8000` in your browser
* You can edit this document, or create new `.md` documents and add content, but everything must be in the `docs` folder
* When you are ready to send your changes back, type: `git pull` (to get the most recent changes), then `git add *` to add any new files you created, then `git commit -m "my message"` to add a description of your changes, then `git push` to push your changes to the site
* After a few minutes you will see the changes at [wiki.latinoamericas.tk](https://wiki.latinoamericas.tk)
* Ask UnsolvedCypher if you need help

## Project layout

    mkdocs.yml    # The configuration file.
    docs/
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and other files.
