ㅤ

# Bienvenidos a la wiki de STK Latinoamérica

**STK Latinoamerica** es una serie de servidores creados por gente de la comunidad de STK en latinoamerica para la gente de latinoamerica, este proyecto tiene como objetivo la creacion de servidores que permitan una buena experiencia de juego para las personas de latinoamérica o paises cercanos los cuales generalmente no tienen una buena conexion o una conexion estable para poder jugar.

# requisitos para STK

## minimo

 - GPU:: NVIDIA GeForce 470 GTX, tarjeta de la serie AMD Radeon 6870 HD o Intel HD Graphics 4000; Al menos 512 MB de VRAM (memoria de video).

 - CPU:  cualquier procesador Intel o AMD de doble núcleo. Los modelos muy antiguos y las partes móviles de baja frecuencia pueden tener problemas, especialmente en el juego en línea.

## recomendado

 - GPU:  NVIDIA GeForce GTX 950, AMD Radeon RX 460 o más fuerte; Al menos 1 GB de VRAM (memoria de video).

 - CPU:  rendimiento de un solo hilo Core i5-2400 o superior. Esto incluye las CPU de escritorio AMD Ryzen, la mayoría de las CPU de escritorio Intel desde 2012 y las CPU móviles recientes de gama media a alta.

## otros requisitos 

 - RAM: Al menos 1GB de RAM libre.

 - DISCO: Al menos 700MB de disco libre.

# paises soportados

actualmente soportamos a los paises 

- **Argentina**
- **Brasil**

por temas de disponibilidad de servidores aun no podemos abrir servidores en mas paises pero lo haremas lo antes posible para que se pueda jugar bien en otros paises como por ejemplo mexico

## tambien ofrecemos

 - **[texturas comprimidas para mayor rendimiento](https://drive.google.com/file/d/1jfOgx_igURjmw0Ok2T_mdcawkXbJgC8U/view?usp=sharing)** 
 - **[crea un servidor personalizado para tus carreras (gratis)](https://wiki..latinoamericas.tk/create_servers.md)**
