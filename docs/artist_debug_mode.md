# que es artist debug mode

artist debug mode es un modo de desarrollo creado para los desarrolladores de pistas, arenas o karts para poder testear sus creaciones con muchas mas funciones las cuales un jugador tradicional no requiere.

# activacion de artist debug mode

## acceder a la carpeta del config.xml de STK

**Windows:**

C:\Users\gonza\AppData\Roaming\supertuxkart\config-0.10

**linux:**

/home/(usuario)/.config/supertuxkart/config-0.10/

**android:**

/storage/emulated/0/android/data/org.supertuxkart.stk/files/supertuxkart/home/supertuxkart/config-0.10/

**mac:** proximamente

![](ADM1.PNG)

## editar config.xml

accedemos a config.xml con un editor de texto

![](ADM2.PNG)

entramos a la seccion de busqueda y buscamos **artist_debug_mode**

![](ADM3.PNG)

cerca de ese **artist_debug_mode** encontraremos el termino **false** como se muestra en la siguiente foto el cual indica el estado de **artist_debug_mode**

 - **false** desactivado
 - **true** activado

![](ADM4.PNG)

remplazamos **false** por **true**

![](ADM5.PNG)

guardamos el archivo 

![](ADM6.PNG)

# uso del artist debug mode

dentro de STK si hacemos click derecho se nos desplegará el menu del **artist_debug_mode**

![](ADM7.PNG)

## funciones del artist debug mode

 - mayor informacion al presionar F12
 - inicio de carreras offline automaticamente
 - menu de opciones avanzadas

![](ADM8.png)

este menu permite modifficar varias cosas por ejemplo quitar el limite de FPS, ver la hitbox de todo en el mapay lo que este dentro de el, cambiar la posicion de la camara completamente, deshabilitar los iconos que aparecen en las carreras y la otorgacion de itemos o de trampas son algunas de las funciones de este menu.







 
