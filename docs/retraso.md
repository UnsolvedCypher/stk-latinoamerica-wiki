
en los uegos en linea el retraso es un retraso notable entre la accion de los jugadores y la reaccion del servidor.

Esto conduce a teletransportes inesperados, por lo que los jugadores no están realmente donde se ven. Debido a eso, pueden ocurrir choques involuntarios e inesperados contra, por ejemplo, paredes u otros karts y un jugador pierde tiempo y tal vez incluso la carrera debido a esto. Los retrasos son especialmente indeseables en las clasificatorias o en cualquier otro tipo de juego competitivo.

Los retrasos aparecen particularmente cuando se tiene un ping alto, lo que significa estar lejos del servidor. Sin embargo, el ping no es el factor decisivo; El jitter (también conocido como pérdida de paquetes) tiene una mayor influencia en el retraso. Por ejemplo, si los paquetes no se pierden, un jugador puede tener una experiencia de juego relativamente buena mientras está en el otro lado del mundo, pero si la conexión es inestable, incluso los servidores cercanos pueden producir retrasos para un jugador.

Tenga en cuenta que las redes inalámbricas hacen que se pierdan más paquetes, lo que significa que se recomienda la conexión por cable, especialmente para carreras clasificadas o competencias populares.

gracias a kimden por el contenido original.

